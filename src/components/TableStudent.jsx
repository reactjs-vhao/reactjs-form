import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteProduct, editProduct } from "../store/formSlice";

const TableStudent = () => {
  const { productList } = useSelector((state) => state.formReducer);
  const [searchText, setSearchText] = useState("");
  const [searchResults, setSearchResults] = useState();
  const handleChange = (event) => {
    setSearchText(event.target.value);
    const result = productList.filter((pr) =>
      pr.name.toLowerCase().includes(searchText.toLowerCase())
    );
    setSearchResults(result);
  };
  const listDisplay = searchText ? searchResults : productList;

  const dispatch = useDispatch();
  return (
    <div className="mt-3 container">
      <div className=" form-group mb-3 row d-flex align-items-center">
        <h5 className="col-1 bg-dark text-warning py-1 mt-1">Tìm kiếm</h5>
        <div className="col-11">
          <input
            className="form-control"
            type="search"
            placeholder="Nhập tên cần tìm..."
            value={searchText}
            onChange={handleChange}
          />
        </div>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th className="bg-dark text-warning">Mã SV</th>
            <th className="bg-dark text-warning">Họ Tên</th>
            <th className="bg-dark text-warning">Số điện thoại</th>
            <th className="bg-dark text-warning">Email</th>
            <th className="bg-dark text-warning">Chức năng</th>
          </tr>
        </thead>
        <tbody>
          {listDisplay?.map((prd) => (
            <tr key={prd?.id}>
              <td>{prd?.id}</td>
              <td>{prd?.name}</td>
              <td>{prd?.number}</td>
              <td>{prd?.email}</td>
              <td>
                <button
                  className="btn btn-primary"
                  onClick={() => {
                    dispatch(editProduct(prd));
                  }}
                >
                  <i className="fa-solid fa-user-pen"></i>
                </button>
                <button
                  className="btn btn-danger ms-2"
                  onClick={() => {
                    dispatch(deleteProduct(prd.id));
                  }}
                >
                  <i className="fa-solid fa-trash-can"></i>
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableStudent;
