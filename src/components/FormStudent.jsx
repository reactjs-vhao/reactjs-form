import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addProduct, updateProduct } from "../store/formSlice";

const FormStudent = () => {
  const [formData, setFormData] = useState();
  const [formError, setFormError] = useState();
  const dispatch = useDispatch();
  const { productEdit, productList } = useSelector(
    (state) => state.formReducer
  );
  const handleFormData = () => (e) => {
    const { name, value, minLength, max, title, validity } = e.target;
    let messager;
    if (minLength !== -1 && !value.length) {
      messager = `Vui lòng nhập thông tin ${title}`;
    } else if (value.length < minLength || (value.length > max && max)) {
      messager = `Vui lòng nhập tối thiểu ${minLength} tới ${max} kí tự`;
    } else if (validity.patternMismatch && ["id", "number"].includes(name)) {
      messager = `Vui lòng nhập kí tự là số`;
    } else if (name === "id") {
      const ID = productList?.find((check) => check.id === value);
      if (ID) messager = `Mã sinh viên tồn tại `;
    } else if (validity.patternMismatch && ["email"].includes(name)) {
      messager = `Vui lòng nhập đúng email`;
    }

    setFormError({
      ...formError,
      [name]: messager,
    });
    setFormData({
      ...formData,
      [name]: messager ? undefined : value,
    });
  };
  useEffect(() => {
    if (!productEdit) {
      return;
    }
    setFormData(productEdit);
  }, [productEdit]);
  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        const elements = document.querySelectorAll("input");
        let formError = {};
        elements.forEach((ele) => {
          const { name, value, minLength, max, title, validity } = ele;
          let mes;
          if (minLength !== -1 && !value.length) {
            mes = `Vui lòng nhập thông tin ${title}`;
          } else if (value.length < minLength || (value.length > max && max)) {
            mes = `Vui lòng nhập tối thiểu ${minLength} tới ${max} kí tự`;
          } else if (
            validity.patternMismatch &&
            ["id", "number"].includes(name)
          ) {
            mes = `Vui lòng nhập kí tự là số`;
          } else if (name === "id" && !productEdit) {
            const ID = productList?.find((check) => check.id === value);
            if (ID) mes = `Mã sinh viên tồn tại `;
          } else if (validity.patternMismatch && ["email"].includes(name)) {
            mes = `Vui lòng nhập đúng email`;
          }
          formError[name] = mes;
        });
        let flag = false;
        for (let key in formError) {
          if (formError[key]) {
            flag = true;
            break;
          }
        }
        if (flag) {
          setFormError(formError);
          return;
        }
        if (productEdit) {
          dispatch(updateProduct(formData));
        } else dispatch(addProduct(formData));
      }}
      noValidate
    >
      <h2 className="px-2 py-4 bg-dark text-warning">Thông Tin Sinh Viên</h2>
      <div className="form-group row">
        <div className="col-6">
          <p>Mã SV</p>
          <input
            type="text"
            disabled={Boolean(productEdit)}
            value={formData?.id}
            name="id"
            title="id"
            minLength={3}
            max={6}
            pattern="^[0-9]+$"
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.id}</p>
        </div>
        <div className="col-6">
          <p>Họ tên</p>
          <input
            type="text"
            value={formData?.name}
            name="name"
            title="Họ và tên"
            minLength={1}
            className="form-control"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.name}</p>
        </div>
      </div>
      <div className="form-group row">
        <div className="col-6">
          <p>Số điện thoại</p>
          <input
            type="number"
            value={formData?.number}
            className="form-control"
            name="number"
            title="Số điện thoại"
            minLength={1}
            pattern="^[0-9]+$"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.number}</p>
        </div>
        <div className="col-6">
          <p>Email</p>
          <input
            type="text"
            value={formData?.email}
            className="form-control"
            name="email"
            title="email"
            minLength={1}
            pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.email}</p>
        </div>
      </div>
      <div className="mt-2">
        {productEdit && <button className="btn btn-warning">Cập nhật</button>}
        {!productEdit && <button className="btn btn-success">Thêm</button>}
      </div>
    </form>
  );
};

export default FormStudent;
