import React from "react";
import FormStudent from "../components/FormStudent";
import TableStudent from "../components/TableStudent";

const Form = () => {
  return (
    <div className="container w-75 m-auto mt-5">
      <FormStudent />
      <TableStudent />
    </div>
  );
};

export default Form;
