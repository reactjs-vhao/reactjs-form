import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  productList: [],
  productEdit: undefined,
};

export const formSlice = createSlice({
  name: "Form",
  initialState,
  reducers: {
    addProduct: (state, action) => {
      state.productList.push(action.payload);
    },
    deleteProduct: (state, action) => {
      state.productList = state.productList.filter(
        (prd) => prd.id !== action.payload
      );
    },
    editProduct: (state, action) => {
      state.productEdit = action.payload;
    },
    updateProduct: (state, action) => {
      state.productList = state.productList.map((prd) => {
        if (prd.id === action.payload.id) {
          return action.payload;
        }
        return prd;
      });
      state.productEdit = undefined;
    },
  },
});

export const { addProduct, deleteProduct, editProduct, updateProduct } =
  formSlice.actions;

export default formSlice.reducer;
